Ce script sert à visualiser les données que mon capteur de CO2 produit. 

# Le capteur

Le capteur est l'un de ceux conseillés par projetco2.fr : https://projetco2.fr/capteurs
C'est le AirCO2ntrol 5000.

Ce dernier produit un fichier CSV daté très simple (position --whatever this is-- date, heure, CO2 en ppm,température en °C, taux d'humidité en %).

# Utilisation
Le script est commenté un peu sommairement. Il suffit de changer le nom du fichier pour changer de source de données. Vous pouvez changer les couleurs, modifier les taquets, etc. pour le mettre à votre goût.
Une fois que c'est fait : `gnuplot co2.gnuplot` dans un terminal et hooo un graphe. 

# Lire les données

J'ai ajouté quatre taquets sur le graphe pour qu'on situe si c'est bien ou pas **pour le contexte de la lutte contre le Covid**.
Je me base sur les recommandations qui sont ici : http://nousaerons.fr/#co2. Les fiches explicatives sont très bien.

Pour plus de détails :
> Depuis avril 2021, justement pour limiter le risque de contamination au SARS-CoV-2 par voie aérienne, le Haut Conseil de la Santé Publique (HCSP) préconise pour les Établissements Recevant du Public (ERP) de maintenir la concentration de CO2 en dessous de 800 ppm, et de 600 ppm pour les lieux de restauration. De nombreux pays comme Taiwan, la Corée du Sud, l’Europe (recommandations de l’OMS pour les écoles), l’Espagne, les USA (800 ppm depuis le 2 juin) préconisent aussi des seuils autour de 1000 ou 800 ppm. Par ailleurs, une étude a montré que dès 1400 ppm de CO2  on peut observer une baisse de performance des fonctions cognitives.

Source : https://ducotedelascience.org/qualite-de-lair-dans-les-transports-et-risque-covid-19-comprendre-le-debat/

L'idée est de rester en dessous des 800 dans les salles de classe/amphis/etc pour que les aérosols ne stagnent jamais.

Mon graphe a donc quatre lignes horizontales colorées :
- 400ppm pour l'air extérieur (en vert) ;
- 600ppm (en vert aussi), ce qui est en gros la recommmandation du Haut Conseil de la Santé Publique pour les restaurants et lieux publics où l'on ne peut pas être masqué ;
- 800ppm (en orange), en gros la recommmandation de la même autorité pour les salles de classe et lieux publics où l'on est masqué ;
- 1200ppm (en rouge), c'est le moment qui correspond...à une qualité d'air bonne selon la norme NF EN16798-1:2019, mais en contexte Covid c'est de l'air déjà trop confiné !

# Exemple 
![Exemple de courbe](exemple-courbe.png)
C'est une courbe où je n'ai pas tracé la courbe de températures, seulement le CO2. 
