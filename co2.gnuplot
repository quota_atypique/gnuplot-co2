#Paramètres pour lire le CSV : séparateur et format de la date

set datafile separator ','
set xdata time
set timefmt "%H:%M:%S"
set y2tics #à partir de là paramètres pour dessiner le graphe : on prépare deux ordonnées pour avoir aussi la température si besoin en plus du CO2.
set ytics nomirror
set ylabel "CO2 (ppm)"
set y2label "Temp (°C)"
#commenter la ligne température si on ne la mesure pas dans le graphe
set key autotitle columnhead

#Là c'est pour que les courbes soient CHOLIES.

set style line 100 lt 1 lc rgb "grey" lw 0.5 
set grid ls 100 
set style line 101 lw 2 lt rgb "#26dfd0" 
set style line 102 lw 2 lt rgb "#6699ff" 
set style line 103 lw 3 lt rgb "orange"
set style line 104 lw 3 lt rgb "red"
set style line 105 lw 3 lt rgb "#00cc00"
set style line 106 lw 3 lt rgb "#339966"

#On dessine le graphe, avec des taquets pour les normes pour visualiser où l'on se situe. Deux choix. Ou on monitore le CO2 et la température :
#plot 'fichier.csv' using 3:4 with lines ls 101, '' using 3:5 with lines ls 102 axis x1y2, 410 title "Air extérieur" ls 106, 600 title "Recommandé (restaus)" ls 105, 800 title "Recommandé (salles de classe, réunion, etc.)" ls 103, 1200 title "Trop confiné !" ls 104

#Ou l'on monitore le CO2 seul :
plot 'fichier.csv' using 3:4 with lines ls 101, 410 title "Air extérieur" ls 106, 600 title "Recommandé (restaus)" ls 105, 800 title "Recommandé (salles de classe, réunion, etc.)" ls 103, 1200 title "Trop confiné !" ls 104
pause -1 "Hit any key to continue" #ça c'est pour que gnuplot ne se referme pas immédiatement après avoir été lancé. Ça marche mieux que gnuplot -p, la courbe se rafraîchit mieux donc voilà.
